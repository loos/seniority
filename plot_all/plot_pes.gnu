#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 5.3,7.3 enhanced color \
    font 'Helvetica,22' linewidth 2
set output 'plot_pes.eps'

#set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

if (!exists("MP_LEFT"))   MP_LEFT = 0.15
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.08
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.11
if (!exists("MP_yGAP"))   MP_yGAP = 0.06

set multiplot layout 3,2 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 1 dt 1 lw 2 linecolor rgb "gray60"
set style line 2 dt 1 lw 2 linecolor rgb "black"
set style line 3 dt 1 lw 2 linecolor rgb "light-red"
set style line 4 dt 1 lw 2 linecolor rgb "sea-green"
set style line 5 dt 2 lw 2 linecolor rgb "sea-green"
set style line 6 dt 1 lw 2 linecolor rgb "orange"
set style line 7 dt 2 lw 2 linecolor rgb "orange"
set style line 8 dt 1 lw 2 linecolor rgb "medium-blue"

set label 1  'Distance (a_{0})'         at screen 0.46,0.03              tc ls 2 #font 'Verdana,20'
set label 11 'HF'          at screen 0.34,0.75              tc ls 2 font 'Helvetica,26'
set label 12 'F_2'         at screen 0.79,0.75              tc ls 2 font 'Helvetica,26'
set label 13 'ethylene'    at screen 0.34,0.43              tc ls 2 font 'Helvetica,26'
set label 14 'N_2'         at screen 0.79,0.43              tc ls 2 font 'Helvetica,26'
set label 15 'H_4'         at screen 0.34,0.13              tc ls 2 font 'Helvetica,26'
set label 16 'H_8'         at screen 0.79,0.13              tc ls 2 font 'Helvetica,26'

set format y "%.1f"

set xrange[0.5:6.0]
xmin=0.5
xmax=6.0
# VIEW 1:
#set yrange[-199.11:-198.90]
# VIEW 2:
#set yrange[-100.25:-99.85]
set yrange[-100.25:-99.50]
set ytics 0.2
ymin=-100.25
ymax=-99.85
plot '../HF_cc-pvdz/pes_rhf.dat'   w l ls 1 notitle, \
     '../HF_cc-pvdz/pes_ooCIS.dat' w l ls 3 notitle, \
     '../HF_cc-pvdz/pes_fci.dat'   w l ls 2 notitle
#    '../HF_cc-pvdz/pes_CISD.dat'  w l ls 3 notitle, \
#    '../HF_cc-pvdz/pes_CIo1.dat'  w l ls 4 notitle, \
unset ylabel
unset label

set xrange[0.95:8.0]
xmin=0.95
xmax=8.0
# VIEW 1:
#set yrange[-199.11:-198.90]
# VIEW 2:
#set yrange[-199.11:-198.6]
set yrange[-199.11:-198.3]
set ytics 0.2
ymin=-199.11
ymax=-198.6
plot '../F2_cc-pvdz/pes_rhf.dat'   w l ls 1 notitle, \
     '../F2_cc-pvdz/pes_ooCIS.dat' w l ls 3 notitle, \
     '../F2_cc-pvdz/pes_fci.dat'   w l ls 2 notitle
#    '../F2_cc-pvdz/pes_CISD.dat'  w l ls 3 notitle, \
#    '../F2_cc-pvdz/pes_CIo1.dat'  w l ls 4 notitle, \

set xrange[1.5:16.0]
xmin=1.5
xmax=16.0
# VIEW 1:
#set yrange[-78.40:-77.7]
set yrange[-78.40:-77.3]
set ytics 0.2
ymin=-78.40
ymax=-77.7
set ylabel  'Energy (Hartree)' 
plot '../ethylene_cc-pvdz/pes_rhf.dat'   w l ls 1 notitle, \
     '../ethylene_cc-pvdz/pes_ooCIS.dat' w l ls 3 notitle, \
     '../ethylene_cc-pvdz/pes_fci.dat'   w l ls 2 notitle
#    '../ethylene_cc-pvdz/pes_CISD.dat'  w l ls 3 notitle, \
#    '../ethylene_cc-pvdz/pes_CIo1.dat'  w l ls 4 notitle, \

unset ylabel
set xrange[0.7:4.0]
xmin=0.7
xmax=4.0
# VIEW 1:
#set yrange[-199.11:-198.90]
# VIEW 2:
#set yrange[-109.30:-108.30]
set yrange[-109.30:-107.80]
set ytics 0.4
ymin=-109.30
ymax=-108.30
plot '../N2_cc-pvdz/pes_rhf.dat'   w l ls 1 notitle, \
     '../N2_cc-pvdz/pes_ooCIS.dat' w l ls 3 notitle, \
     '../N2_cc-pvdz/pes_fci.dat'   w l ls 2 notitle
#    '../N2_cc-pvdz/pes_CISD.dat'  w l ls 3 notitle, \
#    '../N2_cc-pvdz/pes_CIo1.dat'  w l ls 4 notitle, \

set xrange[1.0:10.0]
xmin=1.0
xmax=10.0
# VIEW 1:
#set yrange[-2.3:-1.7]
set yrange[-2.3:-1.5]
set ytics 0.2
ymin=-2.3
ymax=-1.7
plot '../H4_cc-pvdz/pes_rhf.dat'   w l ls 1 notitle, \
     '../H4_cc-pvdz/pes_ooCIS.dat' w l ls 3 notitle, \
     '../H4_cc-pvdz/pes_fci.dat'   w l ls 2 notitle
#    '../H4_cc-pvdz/pes_CISD.dat'  w l ls 3 notitle, \
#    '../H4_cc-pvdz/pes_CIo1.dat'  w l ls 4 notitle, \

set xrange[1.0:10.0]
xmin=1.0
xmax=10.0
# VIEW 1:
set yrange[-4.6:-3.0]
set ytics 0.4
ymin=-4.6
ymax=-3.0
plot '../H8_cc-pvdz/pes_rhf.dat'   w l ls 1 notitle, \
     '../H8_cc-pvdz/pes_ooCIS.dat' w l ls 3 notitle, \
     '../H8_cc-pvdz/pes_fci.dat'   w l ls 2 notitle
#    '../H8_cc-pvdz/pes_CISD.dat'  w l ls 3 notitle, \
#    '../H8_cc-pvdz/pes_CIo1.dat'  w l ls 4 notitle, \
