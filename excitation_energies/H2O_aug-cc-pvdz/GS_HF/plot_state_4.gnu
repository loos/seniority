#!/bin/gnuplot

efci4(x,y) = 9.94

filename4 = 'map_seniority_vs_excitation_state_4.dat'

set zrange[9.5:13.5]

set terminal wxt enhanced size 400,600

unset key

set ticslevel 0
set xlabel 'seniority'
set ylabel 'excitation'
set zlabel 'Excitation energy (eV)' rotate parallel offset 0,0,0
set xtics 2
set ytics 1

set view 74,159

#set xyplane at 0
#set grid x y z vertical linewidth 1
set style fill transparent solid 0.25

set style arrow 1 heads size screen 0.005, 90
set style arrow 1 lc "violet" lw 1.5
set style arrow 2 heads size screen 0.005, 90
set style arrow 2 lc "dark-green" lw 1.5

set samples 2
set isosamples 2

splot filename4 using 1:2:3 with lines, \
      '' using 1:2:($3-$4):(0):(0):(2.*$4) with vector as 2, \
      efci4(x,y) w l

pause -1
