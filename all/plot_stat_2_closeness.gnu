#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 5.3,5.3 enhanced color \
    font 'Helvetica,24' linewidth 2
set output 'plot_stat_2_closeness.eps'


###################################################################################
###################################################################################
# SYSTEM DEPENDENT PART:
set xrange[-0.4:7.7]
#set yrange[1e-3:1]
#set logscale y
#set format y "10^{%T}"
set yrange[0.0:0.5]

###################################################################################
###################################################################################

set xtics 1
set mxtics 2

set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

if (!exists("MP_LEFT"))   MP_LEFT = 0.13
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.98
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.15
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.08
if (!exists("MP_yGAP"))   MP_yGAP = 0.08

set multiplot layout 1,2 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

#set style line 3  dt 1 lw 2 linecolor rgb "light-red" pt 13 ps 2
#set style line 4  dt 1 lw 2 linecolor rgb "sea-green" pt 13 ps 2
#set style line 13 dt 1 lw 2 linecolor rgb "light-red" pt 7  ps 2
#set style line 14 dt 1 lw 2 linecolor rgb "sea-green" pt 7  ps 2

set style line 3 lc rgb "light-red"
set style line 4 lc rgb "sea-green"

set style fill solid
set boxwidth 0.3
s=0.35

set xtics ( \
"Be_{2}" 0.0, \
"ethylene" 1.0, \
"F_{2}" 2.0, \
"H_{2}O" 3.0, \
"H_{4}" 4.0, \
"H_{8}" 5.0, \
"HF" 6.0, \
"N_{2}" 7.0) \
rotate by 90 right

set ytics rotate by 90 right

#plot "data.dat" every ::0::0 using 1:3:xtic(2) with boxes ls 1, \
#     "data.dat" every ::1::2 using 1:3:xtic(2) with boxes ls 2

#set xlabel 'Computational scaling'
set ylabel 'Closeness error (Hartree)'

plot 'stat_CISD.dat'  u ($0):($3)    w boxes ls 3 notitle, \
     'stat_CIo2.dat'  u ($0+s):($3)  w boxes ls 4 notitle

unset ylabel
unset label

plot 'stat_ooCISD.dat'  u ($0):($3)    w boxes ls 3 notitle, \
     'stat_ooCIo2.dat'  u ($0+s):($3)  w boxes ls 4 notitle
