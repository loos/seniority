#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 7.3,7.3 enhanced color \
    font 'Helvetica,22' linewidth 2
set output 'plot_pes_HF.eps'
set encoding iso_8859_1

#set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

if (!exists("MP_LEFT"))   MP_LEFT = 0.12
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.08
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.03
if (!exists("MP_yGAP"))   MP_yGAP = 0.06

set multiplot layout 2,3 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 1 dt 1 lw 2 linecolor rgb "gray60"
set style line 2 dt 1 lw 2 linecolor rgb "black"
set style line 3 dt 1 lw 2 linecolor rgb "light-red"
set style line 4 dt 1 lw 2 linecolor rgb "sea-green"
set style line 5 dt 2 lw 2 linecolor rgb "sea-green"
set style line 6 dt 1 lw 2 linecolor rgb "orange"
set style line 7 dt 2 lw 2 linecolor rgb "orange"
set style line 8 dt 1 lw 2 linecolor rgb "medium-blue"

set format y "%.1f"
set xrange[0.5:6.0]
set yrange[-100.40:-99.80]
set ytics 0.1
set ylabel 'Energy (Hartree)'

plot '../HF_cc-pvdz/pes_rhf.dat'    w l ls 1 notitle, \
     '../HF_cc-pvdz/pes_CISD.dat'   w l ls 3 notitle, \
     '../HF_cc-pvdz/pes_CISDT.dat'  w l ls 3 notitle, \
     '../HF_cc-pvdz/pes_s0.dat'     w l ls 8 notitle, \
     '../HF_cc-pvdz/pes_s2.dat'     w l ls 8 notitle, \
     '../HF_cc-pvdz/pes_s4.dat'     w l ls 8 notitle, \
     '../HF_cc-pvdz/pes_CIo1.dat'   w l ls 4 notitle, \
     '../HF_cc-pvdz/pes_CIo1.5.dat' w l ls 5 notitle, \
     '../HF_cc-pvdz/pes_CIo2.dat'   w l ls 4 notitle, \
     '../HF_cc-pvdz/pes_CIo2.5.dat' w l ls 5 notitle, \
     '../HF_cc-pvdz/pes_CIo3.dat'   w l ls 4 notitle, \
     '../HF_cc-pvdz/pes_fci.dat'    w l ls 2 notitle
unset ylabel
set format y ""

plot '../HF_cc-pvtz/pes_rhf.dat'    w l ls 1 notitle, \
     '../HF_cc-pvtz/pes_CISD.dat'   w l ls 3 notitle, \
     '../HF_cc-pvtz/pes_CISDT.dat'  w l ls 3 notitle, \
     '../HF_cc-pvtz/pes_s0.dat'     w l ls 8 notitle, \
     '../HF_cc-pvtz/pes_s2.dat'     w l ls 8 notitle, \
     '../HF_cc-pvtz/pes_s4.dat'     w l ls 8 notitle, \
     '../HF_cc-pvtz/pes_CIo1.dat'   w l ls 4 notitle, \
     '../HF_cc-pvtz/pes_CIo1.5.dat' w l ls 5 notitle, \
     '../HF_cc-pvtz/pes_CIo2.dat'   w l ls 4 notitle, \
     '../HF_cc-pvtz/pes_CIo2.5.dat' w l ls 5 notitle, \
     '../HF_cc-pvtz/pes_CIo3.dat'   w l ls 4 notitle, \
     '../HF_cc-pvtz/pes_fci.dat'    w l ls 2 notitle

plot '../HF_cc-pvqz/pes_rhf.dat'    w l ls 1 notitle, \
     '../HF_cc-pvqz/pes_CISD.dat'   w l ls 3 notitle, \
     '../HF_cc-pvqz/pes_CISDT.dat'  w l ls 3 notitle, \
     '../HF_cc-pvqz/pes_s0.dat'     w l ls 8 notitle, \
     '../HF_cc-pvqz/pes_s2.dat'     w l ls 8 notitle, \
     '../HF_cc-pvqz/pes_s4.dat'     w l ls 8 notitle, \
     '../HF_cc-pvqz/pes_CIo1.dat'   w l ls 4 notitle, \
     '../HF_cc-pvqz/pes_CIo1.5.dat' w l ls 5 notitle, \
     '../HF_cc-pvqz/pes_CIo2.dat'   w l ls 4 notitle, \
     '../HF_cc-pvqz/pes_CIo2.5.dat' w l ls 5 notitle, \
     '../HF_cc-pvqz/pes_CIo3.dat'   w l ls 4 notitle, \
     '../HF_cc-pvqz/pes_fci.dat'    w l ls 2 notitle

set xrange[0.5:6.0]
set yrange[0.0:0.35]
set format y "%.2f"
set ylabel 'Energy error (Hartree)'

plot '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_rhf.dat'    using 1:($4-$2) w l ls 1 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CISD.dat'   using 1:($4-$2) w l ls 3 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CISDT.dat'  using 1:($4-$2) w l ls 3 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_s0.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_s2.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_s4.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CIo1.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CIo1.5.dat' using 1:($4-$2) w l ls 5 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CIo2.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CIo2.5.dat' using 1:($4-$2) w l ls 5 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_CIo3.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_fci.dat'    using 1:($4-$2) w l ls 2 notitle
unset ylabel
set format y ""

set xlabel "Distance ({\305})"
plot '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_rhf.dat'    using 1:($4-$2) w l ls 1 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CISD.dat'   using 1:($4-$2) w l ls 3 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CISDT.dat'  using 1:($4-$2) w l ls 3 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_s0.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_s2.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_s4.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CIo1.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CIo1.5.dat' using 1:($4-$2) w l ls 5 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CIo2.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CIo2.5.dat' using 1:($4-$2) w l ls 5 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_CIo3.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvtz/pes_fci.dat ../HF_cc-pvtz/pes_fci.dat'    using 1:($4-$2) w l ls 2 notitle
unset xlabel

plot '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_rhf.dat'    using 1:($4-$2) w l ls 1 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CISD.dat'   using 1:($4-$2) w l ls 3 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CISDT.dat'  using 1:($4-$2) w l ls 3 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_s0.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_s2.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_s4.dat'     using 1:($4-$2) w l ls 8 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CIo1.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CIo1.5.dat' using 1:($4-$2) w l ls 5 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CIo2.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CIo2.5.dat' using 1:($4-$2) w l ls 5 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_CIo3.dat'   using 1:($4-$2) w l ls 4 notitle, \
     '< paste ../HF_cc-pvqz/pes_fci.dat ../HF_cc-pvqz/pes_fci.dat'    using 1:($4-$2) w l ls 2 notitle

