#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 5.5,7.3 enhanced color \
    font 'Helvetica,22' linewidth 2
set output 'freq_HF.eps'
set encoding iso_8859_1

###################################################################################
###################################################################################
# SYSTEM DEPENDENT PART:
#set yrange[1e-2:1]
#set logscale y
#set format y "10^{%T}"
#set yrange[0:1]

set xrange[1:1e9]
set logscale x
set format x "10^{%T}"

###################################################################################
###################################################################################

set xtics 10**3
set mxtics 1

#set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

#if (!exists("MP_LEFT"))   MP_LEFT = 0.14
#if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
#if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.09
#if (!exists("MP_TOP"))    MP_TOP = 0.97
#if (!exists("MP_xGAP"))   MP_xGAP = 0.15
#if (!exists("MP_yGAP"))   MP_yGAP = 0.07

if (!exists("MP_LEFT"))   MP_LEFT = 0.13
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.98
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.09
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.16
if (!exists("MP_yGAP"))   MP_yGAP = 0.07


set multiplot layout 3,2 columnsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 2  dt 1 lw 2.0 linecolor rgb "black"
set style line 3  dt 1 lw 2.0 linecolor rgb "light-red"   pt 13 ps 2.0
set style line 4  dt 1 lw 2.0 linecolor rgb "sea-green"   pt 13 ps 2.0
set style line 8  dt 1 lw 2.0 linecolor rgb "medium-blue" pt 13 ps 2.0
set style line 13 dt 1 lw 2.0 linecolor rgb "light-red"   pt 7  ps 2.0
set style line 14 dt 1 lw 2.0 linecolor rgb "sea-green"   pt 7  ps 2.0
set style line 18 dt 1 lw 2.0 linecolor rgb "medium-blue" pt 7  ps 2.0

#set label 1  'Number of determinants'           at screen 0.40,0.03              tc ls 2 #font 'Verdana,20'
set label 11 'cc-pVDZ'          at screen 0.32,0.94              tc ls 2 font 'Helvetica,26'
set label 12 'cc-pVTZ'          at screen 0.32,0.62              tc ls 2 font 'Helvetica,26'
set label 13 'cc-pVQZ'          at screen 0.32,0.31              tc ls 2 font 'Helvetica,26'
set label 21 'cc-pVDZ'          at screen 0.82,0.94              tc ls 2 font 'Helvetica,26'
set label 22 'cc-pVTZ'          at screen 0.82,0.62              tc ls 2 font 'Helvetica,26'
set label 23 'cc-pVQZ'          at screen 0.82,0.31              tc ls 2 font 'Helvetica,26'

hartree = 4.3597447222071e-18  # joules
bohr    = 1./18897161646.321   # m
amu     = 1.6605402e-27        # kg
c       = 299792458.0          # m/s
mole    = 6.02214076e23


set format y "%.0f"

set xrange[1:1e9]
set xtics 10**2
set yrange[4000:4700]
set ytics 200
nel=8
nel=1
mass1=1.0078250321
mass2=18.9984032
mu=mass1*mass2/(mass1+mass2)*amu
fac = sqrt(hartree/mu)/(2.0*pi*c) * 0.01 * 10**10
plot '../HF_cc-pvdz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../HF_cc-pvdz/det_aD_CI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../HF_cc-pvdz/det_aD_CIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../HF_cc-pvdz/det_aD_CIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle
unset ylabel
unset label


set xrange[1:1e9]
set xtics 10**2
set yrange[4000:4700]
set ytics 200
#set format y ""
set ylabel 'Vibrational frequency (cm^{-1})' 
plot '../HF_cc-pvtz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../HF_cc-pvtz/det_aD_CI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../HF_cc-pvtz/det_aD_CIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../HF_cc-pvtz/det_aD_CIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle
unset ylabel
unset label


set xrange[1:1e9]
set xtics 10**2
set yrange[4000:4700]
set ytics 200
set xlabel 'Number of determinants'
plot '../HF_cc-pvqz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../HF_cc-pvqz/det_aD_CI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../HF_cc-pvqz/det_aD_CIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../HF_cc-pvqz/det_aD_CIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle
unset xlabel
unset ylabel
unset label


set xrange[1:1e9]
set logscale x
set format x "10^{%T}"
set xtics 10**2
set mxtics 1
set yrange[0.89:0.93]
set format y "%.2f"
set ytics 0.01

plot '../HF_cc-pvdz/det_xe_FCI.dat'     u 2:3 w l  ls 2  notitle, \
     '../HF_cc-pvdz/det_xe_CI.dat'      u 1:3 w lp ls 3  notitle, \
     '../HF_cc-pvdz/det_xe_CIs.dat'     u 1:3 w lp ls 8  notitle, \
     '../HF_cc-pvdz/det_xe_CIo.dat'     u 1:3 w lp ls 4  notitle

set ylabel "Equilibrium distance ({\305})"
plot '../HF_cc-pvtz/det_xe_FCI.dat'     u 2:3 w l  ls 2  notitle, \
     '../HF_cc-pvtz/det_xe_CI.dat'      u 1:3 w lp ls 3  notitle, \
     '../HF_cc-pvtz/det_xe_CIs.dat'     u 1:3 w lp ls 8  notitle, \
     '../HF_cc-pvtz/det_xe_CIo.dat'     u 1:3 w lp ls 4  notitle
unset ylabel

set xlabel 'Number of determinants'
plot '../HF_cc-pvqz/det_xe_FCI.dat'     u 2:3 w l  ls 2  notitle, \
     '../HF_cc-pvqz/det_xe_CI.dat'      u 1:3 w lp ls 3  notitle, \
     '../HF_cc-pvqz/det_xe_CIs.dat'     u 1:3 w lp ls 8  notitle, \
     '../HF_cc-pvqz/det_xe_CIo.dat'     u 1:3 w lp ls 4  notitle

