#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 5.3,7.3 enhanced color \
    font 'Helvetica,22' linewidth 2
set output 'freq_opt.eps'
set encoding iso_8859_1

###################################################################################
###################################################################################
# SYSTEM DEPENDENT PART:
#set yrange[1e-2:1]
#set logscale y
#set format y "10^{%T}"
#set yrange[0:1]

set xrange[1:1e9]
set logscale x
set format x "10^{%T}"

###################################################################################
###################################################################################

set xtics 10**3
set mxtics 1

#set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

if (!exists("MP_LEFT"))   MP_LEFT = 0.15
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.09
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.10
if (!exists("MP_yGAP"))   MP_yGAP = 0.07

set multiplot layout 3,2 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 2  dt 1 lw 1.5 linecolor rgb "black"
set style line 3  dt 1 lw 1.5 linecolor rgb "light-red"   pt 13 ps 1.5
set style line 4  dt 1 lw 1.5 linecolor rgb "sea-green"   pt 13 ps 1.5
set style line 8  dt 1 lw 1.5 linecolor rgb "medium-blue" pt 13 ps 1.5
set style line 13 dt 1 lw 1.5 linecolor rgb "light-red"   pt 7  ps 1.5
set style line 14 dt 1 lw 1.5 linecolor rgb "sea-green"   pt 7  ps 1.5
set style line 18 dt 1 lw 1.5 linecolor rgb "medium-blue" pt 7  ps 1.5

set label 1  'Number of determinants'           at screen 0.40,0.03              tc ls 2 #font 'Verdana,20'
set label 2  'Vibrational frequency (cm^{-1})'  at screen 0.03,0.71 rotate by 90 tc ls 2 #font 'Verdana,20'
set label 3  'Vibrational frequency (cm^{-1})'  at screen 0.03,0.40 rotate by 90 tc ls 2 #font 'Verdana,20'
set label 4  'Force constant (Hartree/a_{0}^2)' at screen 0.03,0.08 rotate by 90 tc ls 2 #font 'Verdana,20'
set label 11 'HF'          at screen 0.35,0.94              tc ls 2 font 'Helvetica,26'
set label 12 'F_2'         at screen 0.79,0.94              tc ls 2 font 'Helvetica,26'
set label 13 'ethylene'    at screen 0.35,0.63              tc ls 2 font 'Helvetica,26'
set label 14 'N_2'         at screen 0.79,0.63              tc ls 2 font 'Helvetica,26'
set label 15 'H_4'         at screen 0.35,0.315             tc ls 2 font 'Helvetica,26'
set label 16 'H_8'         at screen 0.79,0.315             tc ls 2 font 'Helvetica,26'

hartree = 4.3597447222071e-18  # joules
bohr    = 1./18897161646.321   # m
amu     = 1.6605402e-27        # kg
c       = 299792458.0          # m/s
mole    = 6.02214076e23


set format y "%.0f"

set xrange[1:1e7]
set xtics 10**2
set yrange[4000:4700]
set ytics 200
nel=8
nel=1
mass1=1.0078250321
mass2=18.9984032
mu=mass1*mass2/(mass1+mass2)*amu
fac = sqrt(hartree/mu)/(2.0*pi*c) * 0.01 * 10**10
plot '../HF_cc-pvdz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../HF_cc-pvdz/det_aD_ooCI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../HF_cc-pvdz/det_aD_ooCIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../HF_cc-pvdz/det_aD_ooCIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle
unset ylabel
unset label

set xrange[1:1e10]
set yrange[600:1200]
set ytics 100
nel=14
nel=1
mass1=18.9984032
mass2=18.9984032
mu=mass1*mass2/(mass1+mass2)*amu
fac = sqrt(hartree/mu)/(2.0*pi*c) * 0.01 * 10**10
plot '../F2_cc-pvdz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../F2_cc-pvdz/det_aD_ooCI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../F2_cc-pvdz/det_aD_ooCIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../F2_cc-pvdz/det_aD_ooCIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle

set xrange[1:1e11]
#set xtics 10**3
set yrange[780:900]
set ytics 20
nel=12
nel=1
mass1=14.0156500642
mass2=14.0156500642
mu=mass1*mass2/(mass1+mass2)*amu
fac = sqrt(hartree/mu)/(2.0*pi*c) * 0.01 * 10**10
plot '../ethylene_cc-pvdz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../ethylene_cc-pvdz/det_aD_ooCI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../ethylene_cc-pvdz/det_aD_ooCIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../ethylene_cc-pvdz/det_aD_ooCIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle

set xrange[1:1e9]
set yrange[2300:2800]
set ytics 100
nel=10
nel=1
mass1=14.003074
mass2=14.003074
mu=mass1*mass2/(mass1+mass2)*amu
fac = sqrt(hartree/mu)/(2.0*pi*c) * 0.01 * 10**10
plot '../N2_cc-pvdz/det_aD_FCI.dat'     u 2:(sqrt(2*$5)*$3*fac) w l  ls 2  notitle, \
     '../N2_cc-pvdz/det_aD_ooCI.dat'      u 1:(sqrt(2*$5)*$3*fac) w lp ls 3  notitle, \
     '../N2_cc-pvdz/det_aD_ooCIs.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 8  notitle, \
     '../N2_cc-pvdz/det_aD_ooCIo.dat'     u 1:(sqrt(2*$5)*$3*fac) w lp ls 4  notitle

set xrange[1:1e5]
#set xtics 10**2
set yrange[0.58:0.68]
set format y "%.2f"
set ytics 0.02
nel=4
nel=1
plot '../H4_cc-pvdz/det_aD_FCI.dat'     u 2:(2*$3*$3*$5) w l  ls 2  notitle, \
     '../H4_cc-pvdz/det_aD_ooCI.dat'      u 1:(2*$3*$3*$5) w lp ls 3  notitle, \
     '../H4_cc-pvdz/det_aD_ooCIs.dat'     u 1:(2*$3*$3*$5) w lp ls 8  notitle, \
     '../H4_cc-pvdz/det_aD_ooCIo.dat'     u 1:(2*$3*$3*$5) w lp ls 4  notitle

set xrange[1:1e9]
#set xtics 10**3
set yrange[1.14:1.30]
set format y "%.2f"
set ytics 0.05
nel=8
nel=1
plot '../H8_cc-pvdz/det_aD_FCI.dat'     u 2:(2*$3*$3*$5) w l  ls 2  notitle, \
     '../H8_cc-pvdz/det_aD_ooCI.dat'      u 1:(2*$3*$3*$5) w lp ls 3  notitle, \
     '../H8_cc-pvdz/det_aD_ooCIs.dat'     u 1:(2*$3*$3*$5) w lp ls 8  notitle, \
     '../H8_cc-pvdz/det_aD_ooCIo.dat'     u 1:(2*$3*$3*$5) w lp ls 4  notitle
