#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
#set terminal postscript eps size 5.3,3.0 enhanced color \

set terminal postscript eps size 5.3,5.3 enhanced color \
    font 'Helvetica,24' linewidth 2
set output 'plot_pes.eps'
set encoding iso_8859_1

###################################################################################
###################################################################################
# SYSTEM DEPENDENT PART:

set xrange[2.0:10.0]
xmin=2.0
xmax=10.0

# VIEW 1:
set yrange[-29.256:-29.235]
ymin=-29.256
ymax=-29.235

# VIEW 2:
set yrange[-29.26:-29.12]
ymin=-29.26
ymax=-29.12

###################################################################################
###################################################################################

set format y "%.2f"

if (!exists("MP_LEFT"))   MP_LEFT = 0.20
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.10
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.05
if (!exists("MP_yGAP"))   MP_yGAP = 0.08

set multiplot layout 1,2 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 1 dt 1 lw 3 linecolor rgb "gray60"
set style line 2 dt 1 lw 3 linecolor rgb "black"
set style line 3 dt 1 lw 3 linecolor rgb "light-red"
set style line 4 dt 1 lw 3 linecolor rgb "sea-green"
set style line 5 dt 2 lw 3 linecolor rgb "sea-green"
set style line 6 dt 1 lw 3 linecolor rgb "orange"
set style line 7 dt 2 lw 3 linecolor rgb "orange"

set xlabel "Distance ({\305})"
set ylabel 'Energy (Hartree)'

# get the relation of x- and y-range
dx = xmax-xmin
dy = ymax-ymin
s1 = dx/dy
# get ratio of axes
#s2 = 3.0/5.3
s2 = 5.3/5.3#/2
# helper function for getting the rotation angle of the labels in degree
deg(x) = x/pi*180.0
r(x) = deg(atan(s1*s2*x))
# function for fitting
f(x) = a*x+b

###################################################################################
###################################################################################
# SYSTEM DEPENDENT PART:

#fit [2.7:3.0] [*:*] f(x) 'pes_CISD.dat' u 1:($2) via a,b
#set label 2 'CISD' at 2.8,-29.240 rotate by r(a) center tc ls 3 #font 'Verdana,20'
#fit [3.0:4.0] [*:*] f(x) 'pes_CISDT.dat' u 1:($2) via a,b
#set label 3 'CISDT' at 3.5,-29.2418 rotate by r(a) center tc ls 3 #font 'Verdana,20'
#fit [3.7:4.3] [*:*] f(x) 'pes_CISDTQ.dat' u 1:($2) via a,b
#set label 4 'CISDTQ' at 4.0,-29.2492 rotate by r(a) center tc ls 3 #font 'Verdana,20'
#fit [5.0:7.0] [*:*] f(x) 'pes_CIo2.dat' u 1:($2) via a,b
#set label 12 'CIo2' at 7.0,-29.2408  rotate by r(a) center tc ls 4 #font 'Verdana,20'
#fit [5.0:7.0] [*:*] f(x) 'pes_CIo3.dat' u 1:($2) via a,b
#set label 13 'CIo3' at 7.0,-29.2465 rotate by r(a) center tc ls 4 #font 'Verdana,20'
#fit [5.0:7.0] [*:*] f(x) 'pes_CIo4.dat' u 1:($2) via a,b
#set label 14 'CIo4' at 6.0,-29.2485 rotate by r(a) center tc ls 4 #font 'Verdana,20'
#fit [7.0:9.0] [*:*] f(x) 'pes_fci.dat' u 1:($2) via a,b
#set label 20 'FCI' at 8.0,-29.24829 rotate by r(a) center tc ls 2 #font 'Verdana,20'

plot 'pes_rhf.dat'    w l ls 1 notitle, \
     'pes_CISD.dat'  w l ls 3 notitle, \
     'pes_CISDT.dat'  w l ls 3 notitle, \
     'pes_CIo1.dat'   w l ls 4 notitle, \
     'pes_CIo1.5.dat' w l ls 5 notitle, \
     'pes_CIo2.dat'   w l ls 4 notitle, \
     'pes_CIo2.5.dat' w l ls 5 notitle, \
     'pes_CIo3.dat'   w l ls 4 notitle, \
     'pes_fci.dat'    w l ls 2 notitle
#    'pes_CISDTQ.dat'  w l ls 3 notitle, \
#    'pes_CIo4.dat'   w l ls 4 notitle, \

unset label
unset ylabel
set format y ""

plot 'pes_rhf.dat'      w l ls 1 notitle, \
     'pes_ooCISD.dat'   w l ls 3 notitle, \
     'pes_ooCISDT.dat'  w l ls 3 notitle, \
     'pes_ooCIo1.dat'   w l ls 4 notitle, \
     'pes_ooCIo1.5.dat' w l ls 5 notitle, \
     'pes_ooCIo2.dat'   w l ls 4 notitle, \
     'pes_ooCIo2.5.dat' w l ls 5 notitle, \
     'pes_ooCIo3.dat'   w l ls 4 notitle, \
     'pes_fci.dat'      w l ls 2 notitle

###################################################################################
###################################################################################

