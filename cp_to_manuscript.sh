#!/bin/bash

#path='/home/fabris/ongoing_projects/seniority/Manuscript'
path='/home/fabris/seniority/Manuscript'

molecules=( HF F2 ethylene N2 H4 H8 )
#molecules=( ethylene )
#molecules=( H8 )

for mol in "${molecules[@]}"
do

#echo  "${mol}_cc-pvdz/plot_pes.pdf"

cp ${mol}_cc-pvdz/plot_pes.pdf      $path/${mol}_pes.pdf
cp ${mol}_cc-pvdz/plot_error.pdf    $path/${mol}_pes_error.pdf
cp ${mol}_cc-pvdz/plot_stat.pdf     $path/${mol}_npe.pdf
cp ${mol}_cc-pvdz/plot_distance.pdf $path/${mol}_distance.pdf
cp ${mol}_cc-pvdz/freq.pdf          $path/${mol}_freq.pdf
cp ${mol}_cc-pvdz/force.pdf         $path/${mol}_force.pdf
cp ${mol}_cc-pvdz/xe.pdf            $path/${mol}_xe.pdf

done 
