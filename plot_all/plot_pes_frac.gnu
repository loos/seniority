#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 5.3,7.3 enhanced color \
    font 'Helvetica,22' linewidth 2
set output 'plot_pes_frac.eps'

#set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

if (!exists("MP_LEFT"))   MP_LEFT = 0.15
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.08
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.11
if (!exists("MP_yGAP"))   MP_yGAP = 0.06

set multiplot layout 3,2 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 1 dt 1 lw 2 linecolor rgb "gray60"
set style line 2 dt 1 lw 2 linecolor rgb "black"
set style line 3 dt 1 lw 2 linecolor rgb "light-red"
set style line 4 dt 1 lw 2 linecolor rgb "sea-green"
set style line 5 dt 2 lw 2 linecolor rgb "sea-green"
set style line 6 dt 1 lw 2 linecolor rgb "orange"
set style line 7 dt 2 lw 2 linecolor rgb "orange"
set style line 8 dt 1 lw 2 linecolor rgb "medium-blue"

set label 1  'Distance (a_{0})'         at screen 0.46,0.03              tc ls 2 #font 'Verdana,20'
#set label 2  'Non-parallelity error (Hartree)' at screen 0.03,0.35 rotate by 90 tc ls 2 #font 'Verdana,20'
set label 11 'HF'          at screen 0.34,0.75              tc ls 2 font 'Helvetica,26'
set label 12 'F_2'         at screen 0.79,0.75              tc ls 2 font 'Helvetica,26'
set label 13 'ethylene'    at screen 0.34,0.43              tc ls 2 font 'Helvetica,26'
set label 14 'N_2'         at screen 0.79,0.43              tc ls 2 font 'Helvetica,26'
set label 15 'H_4'         at screen 0.34,0.13              tc ls 2 font 'Helvetica,26'
set label 16 'H_8'         at screen 0.79,0.13              tc ls 2 font 'Helvetica,26'

set format y "%.1f"

set xrange[0.5:6.0]
set yrange[0:1.0]
set ytics 0.2
plot '< paste ../HF_cc-pvdz/pes_fci.dat ../HF_cc-pvdz/pes_rhf.dat ../HF_cc-pvdz/pes_ooCIS.dat'  using 1:(($4-$6)/($4-$2)) w l ls 1 notitle
unset ylabel
unset label

set xrange[0.95:8.0]
set yrange[0:1.0]
set ytics 0.2
plot '< paste ../F2_cc-pvdz/pes_fci.dat ../F2_cc-pvdz/pes_rhf.dat ../F2_cc-pvdz/pes_ooCIS.dat'  using 1:(($4-$6)/($4-$2)) w l ls 1 notitle

set xrange[1.5:16.0]
set yrange[0:1.0]
set ytics 0.2
set ylabel  'Energy (Hartree)' 
plot '< paste ../ethylene_cc-pvdz/pes_fci.dat ../ethylene_cc-pvdz/pes_rhf.dat ../ethylene_cc-pvdz/pes_ooCIS.dat'  using 1:(($4-$6)/($4-$2)) w l ls 1 notitle

unset ylabel
set xrange[0.7:4.0]
set yrange[0:1.0]
set ytics 0.2
plot '< paste ../N2_cc-pvdz/pes_fci.dat ../N2_cc-pvdz/pes_rhf.dat ../N2_cc-pvdz/pes_ooCIS.dat'  using 1:(($4-$6)/($4-$2)) w l ls 1 notitle

set xrange[1.0:10.0]
set yrange[0:1.0]
set ytics 0.2
plot '< paste ../H4_cc-pvdz/pes_fci.dat ../H4_cc-pvdz/pes_rhf.dat ../H4_cc-pvdz/pes_ooCIS.dat'  using 1:(($4-$6)/($4-$2)) w l ls 1 notitle

set xrange[1.0:10.0]
set yrange[0:1.0]
plot '< paste ../H8_cc-pvdz/pes_fci.dat ../H8_cc-pvdz/pes_rhf.dat ../H8_cc-pvdz/pes_ooCIS.dat'  using 1:(($4-$6)/($4-$2)) w l ls 1 notitle

pause -1
