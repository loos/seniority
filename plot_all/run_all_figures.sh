#!/bin/bash

plots=( plot_pes plot_stat plot_stat_opt plot_distance plot_distance_opt freq freq_opt xe xe_opt plot_pes_HF plot_stat_HF freq_HF )

for plot in "${plots[@]}"
do

gnuplot ${plot}.gnu
epspdf ${plot}.eps

done

