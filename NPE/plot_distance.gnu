#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'
set terminal postscript eps size 5.3,7.3 enhanced color \
    font 'Helvetica,22' linewidth 2
set output 'plot_distance.eps'

###################################################################################
###################################################################################
# SYSTEM DEPENDENT PART:
#set yrange[1e-2:1]
#set logscale y
#set format y "10^{%T}"
#set yrange[0:1]

set xrange[1:1e9]
set logscale x
set format x "10^{%T}"

###################################################################################
###################################################################################

set xtics 10**3
set mxtics 1

#set grid xtics ytics mxtics mytics #lc rgb 'blue' lt 1, lc rgb 'red' lt 1

if (!exists("MP_LEFT"))   MP_LEFT = 0.13
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.97
if (!exists("MP_BOTTOM")) MP_BOTTOM = 0.09
if (!exists("MP_TOP"))    MP_TOP = 0.97
if (!exists("MP_xGAP"))   MP_xGAP = 0.09
if (!exists("MP_yGAP"))   MP_yGAP = 0.07

set multiplot layout 3,2 rowsfirst \
               margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set style line 2  dt 1 lw 1.5 linecolor rgb "black"
set style line 3  dt 1 lw 1.5 linecolor rgb "light-red"   pt 13 ps 1.5
set style line 4  dt 1 lw 1.5 linecolor rgb "sea-green"   pt 13 ps 1.5
set style line 8  dt 1 lw 1.5 linecolor rgb "medium-blue" pt 13 ps 1.5
set style line 13 dt 1 lw 1.5 linecolor rgb "light-red"   pt 7  ps 1.5
set style line 14 dt 1 lw 1.5 linecolor rgb "sea-green"   pt 7  ps 1.5
set style line 18 dt 1 lw 1.5 linecolor rgb "medium-blue" pt 7  ps 1.5

set label 1  'Number of determinants'   at screen 0.40,0.03              tc ls 2 #font 'Verdana,20'
set label 2  'Distance error (Hartree)' at screen 0.03,0.35 rotate by 90 tc ls 2 #font 'Verdana,20'
set label 11 'HF'          at screen 0.34,0.93              tc ls 2 font 'Helvetica,26'
set label 12 'F_2'         at screen 0.79,0.93              tc ls 2 font 'Helvetica,26'
set label 13 'ethylene'    at screen 0.34,0.62              tc ls 2 font 'Helvetica,26'
set label 14 'N_2'         at screen 0.79,0.62              tc ls 2 font 'Helvetica,26'
set label 15 'H_4'         at screen 0.34,0.31              tc ls 2 font 'Helvetica,26'
set label 16 'H_8'         at screen 0.79,0.31              tc ls 2 font 'Helvetica,26'

set format y "%.1f"


set xrange[1:1e7]
set xtics 10**2
set yrange[0:0.70]
set ytics 0.1
nel=8
nel=1
plot '../HF_cc-pvdz/stat_CI.dat'   u ($3):($5/nel)  w lp ls 3  notitle, \
     '../HF_cc-pvdz/stat_CIs.dat'  u ($3):($5/nel)  w lp ls 8  notitle, \
     '../HF_cc-pvdz/stat_CIo.dat'  u ($3):($5/nel)  w lp ls 4  notitle
unset ylabel
unset label

set xrange[1:1e10]
set yrange[0:1.10]
set ytics 0.2
nel=14
nel=1
plot '../F2_cc-pvdz/stat_CI.dat'   u ($3):($5/nel)  w lp ls 3  notitle, \
     '../F2_cc-pvdz/stat_CIs.dat'  u ($3):($5/nel)  w lp ls 8  notitle, \
     '../F2_cc-pvdz/stat_CIo.dat'  u ($3):($5/nel)  w lp ls 4  notitle

set xrange[1:1e11]
#set xtics 10**3
set yrange[0:1.10]
set ytics 0.2
nel=12
nel=1
plot '../ethylene_cc-pvdz/stat_CI.dat'   u ($3):($5/nel)  w lp ls 3  notitle, \
     '../ethylene_cc-pvdz/stat_CIs.dat'  u ($3):($5/nel)  w lp ls 8  notitle, \
     '../ethylene_cc-pvdz/stat_CIo.dat'  u ($3):($5/nel)  w lp ls 4  notitle

set xrange[1:1e9]
set yrange[0:1.40]
set ytics 0.2
nel=10
nel=1
plot '../N2_cc-pvdz/stat_CI.dat'   u ($3):($5/nel)  w lp ls 3  notitle, \
     '../N2_cc-pvdz/stat_CIs.dat'  u ($3):($5/nel)  w lp ls 8  notitle, \
     '../N2_cc-pvdz/stat_CIo.dat'  u ($3):($5/nel)  w lp ls 4  notitle

set xrange[1:1e5]
#set xtics 10**2
set yrange[0:0.60]
set ytics 0.1
nel=4
nel=1
plot '../H4_cc-pvdz/stat_CI.dat'   u ($3):($5/nel)  w lp ls 3  notitle, \
     '../H4_cc-pvdz/stat_CIs.dat'  u ($3):($5/nel)  w lp ls 8  notitle, \
     '../H4_cc-pvdz/stat_CIo.dat'  u ($3):($5/nel)  w lp ls 4  notitle

set xrange[1:1e9]
#set xtics 10**3
set yrange[0:1.20]
set ytics 0.2
nel=8
nel=1
plot '../H8_cc-pvdz/stat_CI.dat'   u ($3):($5/nel)  w lp ls 3  notitle, \
     '../H8_cc-pvdz/stat_CIs.dat'  u ($3):($5/nel)  w lp ls 8  notitle, \
     '../H8_cc-pvdz/stat_CIo.dat'  u ($3):($5/nel)  w lp ls 4  notitle

