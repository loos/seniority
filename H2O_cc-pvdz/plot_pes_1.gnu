#!/bin/gnuplot

#set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
#set output 'plot_pes.png'

set xrange[0.6:7.0]
set yrange[-76.30:-75.60]
# ZOOM:
#set xrange[0.95:1.35]
#set yrange[-109.29:-109.20]

set style line 1 dt 1 lw 1 linecolor rgb "gray"
set style line 2 dt 1 lw 1 linecolor rgb "red"
set style line 3 dt 1 lw 1 linecolor rgb "green"
set style line 4 dt 1 lw 1 linecolor rgb "blue"
set style line 5 dt 2 lw 1 linecolor rgb "blue"
set style line 6 dt 1 lw 1 linecolor rgb "orange"

set xlabel 'Distance (angstrom)'
set ylabel 'Energy (Hartree)'

plot 'pes_rhf.dat'   w l ls 1 notitle, \
     'pes_s4.e2.dat' w l ls 3 notitle, \
     'pes_s6.e3.dat' w l ls 3 notitle, \
     'pes_s8.e4.dat' w l ls 3 notitle, \
     'pes_s10.e5.dat' w l ls 3 notitle, \
     'pes_s12.e6.dat' w l ls 3 notitle, \
     'pes_CIo1.dat'   w l ls 5 notitle, \
     'pes_CIo1.5.dat' w l ls 5 notitle, \
     'pes_CIo2.dat'   w l ls 4 notitle, \
     'pes_CIo2.5.dat' w l ls 5 notitle, \
     'pes_CIo3.dat'   w l ls 4 notitle, \
     'pes_CIo3.5.dat' w l ls 5 notitle, \
     'pes_CIo4.dat'   w l ls 4 notitle, \
     'pes_pccd.dat'  w l ls 6 notitle, \
     'pes_fci.dat'   w l ls 2 notitle
#    'pes_s0.e2.dat' w l ls 1 notitle, \
#    'pes_s2.e2.dat' w l ls 1 notitle, \
#    'pes_s2.e3.dat' w l ls 1 notitle, \
#    'pes_s4.e3.dat' w l ls 1 notitle, \
#    'pes_s0.e4.dat' w l ls 1 notitle, \
#    'pes_s2.e4.dat' w l ls 1 notitle, \
#    'pes_s4.e4.dat' w l ls 1 notitle, \
#    'pes_s2.e5.dat' w l ls 1 notitle, \
#    'pes_s4.e5.dat' w l ls 1 notitle, \
#    'pes_s6.e5.dat' w l ls 1 notitle, \
#    'pes_s8.e5.dat' w l ls 1 notitle, \
#    'pes_s0.e6.dat' w l ls 1 notitle, \
#    'pes_s2.e6.dat' w l ls 1 notitle, \
#    'pes_s4.e6.dat' w l ls 1 notitle, \
#    'pes_s6.e6.dat' w l ls 1 notitle, \
#    'pes_s8.e6.dat' w l ls 1 notitle, \
#    'pes_s10.e6.dat' w l ls 1 notitle, \

pause -1
