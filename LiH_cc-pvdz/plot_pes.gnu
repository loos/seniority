#!/bin/gnuplot

set terminal pngcairo size 600,600 enhanced font 'Verdana,10'
set output 'plot_pes.png'

set xrange[0.5:12.0]
set yrange[-8.05:-7.85]
# ZOOM:
#set xrange[2.0:12.0]
#set yrange[-100.05:-99.94]

set style line 1 dt 1 lw 1 linecolor rgb "gray"
set style line 2 dt 1 lw 1 linecolor rgb "red"
set style line 3 dt 1 lw 1 linecolor rgb "green"
set style line 4 dt 1 lw 1 linecolor rgb "blue"
set style line 5 dt 2 lw 1 linecolor rgb "blue"
set style line 6 dt 1 lw 1 linecolor rgb "orange"

set xlabel 'Distance (angstrom)'
set ylabel 'Energy (Hartree)'

plot 'pes_rhf.dat'   w l ls 1 notitle, \
     'pes_s0.e2.dat' w l ls 1 notitle, \
     'pes_s2.e2.dat' w l ls 1 notitle, \
     'pes_s2.e3.dat' w l ls 1 notitle, \
     'pes_s4.e3.dat' w l ls 1 notitle, \
     'pes_s0.e4.dat' w l ls 1 notitle, \
     'pes_s2.e4.dat' w l ls 1 notitle, \
     'pes_s4.e4.dat' w l ls 1 notitle, \
     'pes_CISD.dat' w l ls 3 notitle, \
     'pes_CISDT.dat' w l ls 3 notitle, \
     'pes_CISDTQ.dat' w l ls 3 notitle, \
     'pes_CIo1.dat'   w l ls 5 notitle, \
     'pes_CIo1.5.dat' w l ls 5 notitle, \
     'pes_CIo2.dat'   w l ls 4 notitle, \
     'pes_CIo2.5.dat' w l ls 5 notitle, \
     'pes_CIo3.dat'   w l ls 4 notitle, \
     'pes_fci.dat'   w l ls 2 notitle

#pause -1
