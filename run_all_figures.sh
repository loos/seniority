#!/bin/bash

molecules=( HF F2 ethylene N2 H4 H8 )

for mol in "${molecules[@]}"
do

cd ${mol}_cc-pvdz

gnuplot plot_pes.gnu
epspdf plot_pes.eps

gnuplot plot_error.gnu
epspdf plot_error.eps

gnuplot plot_stat.gnu
epspdf plot_stat.eps

gnuplot plot_distance.gnu
epspdf plot_distance.eps

gnuplot freq.gnu
epspdf freq.eps

gnuplot xe.gnu
epspdf xe.eps

gnuplot force.gnu
epspdf force.eps

cd ..

done 
